FROM maven:3.8.1-openjdk-11

USER root

WORKDIR /app

COPY . /app

RUN ./mvnw package\
    java -Dserver.port=8086 -jar target/*.jar

EXPOSE 8086

CMD ["java", "-Dserver.port=8086", "-jar", "target/*.jar"]
